import React from 'react';
import {render} from 'react-dom';
import { browserHistory, Router, Route } from 'react-router'
import Start from './components/Start';
import Table from './components/Table';

render(
  <Router history={browserHistory}>
    <Route path="/" component={Start}>
      <Route path="/table" component={Table}></Route>
    </Route>
  </Router>,
  document.getElementById('root')
);
