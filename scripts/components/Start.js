import React, {Component} from 'react';
import io from 'socket.io-client';

const socket = io('//localhost:3000');

export default class Start extends Component {
  constructor(props) {
    super(props)
    this.state = {
      socket: socket,
      maxPlayers: 2,
    }

    this.letBegin = this.letBegin.bind(this);
    socket.on('createTable', this.goToPlay.bind(this));
  }

  goToPlay(table) {
    console.log(table);
  }

  setMaxPlayers(maxPlayers) {
    this.setState({maxPlayers})
  }

  letBegin() {
    this.state.socket.emit('createTable', {maxPlayers: this.state.maxPlayers})
  }

  render() {
    let maxPlayers = [2,3,4,5,6].map(function(maxPlayer, index) {
        const className = this.state.maxPlayers === maxPlayer ? 'active' : '';
        return (<li key={index}  className={className} onClick={this.setMaxPlayers.bind(this, maxPlayer)}>{maxPlayer}</li>)
    }.bind(this));

    return (
      <div>
        <button onClick={this.letBegin}>Начать игру</button>
        <h1>Количество игроков</h1>
        <ul>
          {maxPlayers}
        </ul>
      </div>
    );
  }
}
